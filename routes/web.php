<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->get('/professors', 'ProfessorController@index');
$router->post('/professors', 'ProfessorController@store');
$router->get('/professors/{professors}', 'ProfessorController@show');
$router->put('/professors/{professors}', 'ProfessorController@update');
$router->patch('/professors/{professors}', 'ProfessorController@update');
$router->delete('/professors/{professors}', 'ProfessorController@destroy');

$router->get('/students', 'StudentController@index');
$router->post('/students', 'StudentController@store');
$router->get('/students/{students}', 'StudentController@show');
$router->put('/students/{students}', 'StudentController@update');
$router->patch('/students/{students}', 'StudentController@update');
$router->delete('/students/{students}', 'StudentController@destroy');

$router->get('/courses', 'CourseController@index');
$router->get('/courses/{courses}', 'CourseController@show');

$router->get('/professors/{professors}/courses', 'ProfessorCourseController@index');
$router->post('/professors/{professors}/courses', 'ProfessorCourseController@store');
$router->put('/professors/{professors}/courses/{courses}', 'ProfessorCourseController@update');
$router->patch('/professors/{professors}/courses/{courses}', 'ProfessorCourseController@update');
$router->delete('/professors/{professors}/courses/{courses}', 'ProfessorCourseController@destroy');

$router->get('/courses/{courses}/students', 'CourseStudentController@index');
$router->post('/courses/{courses}/students/{students}', 'CourseStudentController@store');
$router->delete('/courses/{courses}/students/{students}', 'CourseStudentController@destroy');
