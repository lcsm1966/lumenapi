<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 *
 */
class Professor extends Model
{
    protected $fillable = [
        'name',
        'address',
        'phone',
        'profession'

    ];

    protected $hidden = [		
    	'id',
        'created_at',
        'updated_at'
    ];

    public function courses()
    {
    	return $this->hasMany('App\Course');
    }

}
