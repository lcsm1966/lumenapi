<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 *
 */
class Course extends Model
{
    protected $fillable = [
        'title',
        'description',
        'value',
        'professor_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function professor()
    {
        return $this->belongsTo('App\Professor');
    }

    public function students()
    {
        return $this->belongsToMany('App\Student');
    }
}
