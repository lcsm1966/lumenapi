<?php
namespace App\Http\Controllers;

use App\Professor;
use App\Course;
use Illuminate\Http\Request;

class ProfessorCourseController extends Controller
{
    public function index($professor_id)
    {
        $professor = Professor::find($professor_id);

        if ($professor) {
            $courses = $professor->courses;

            return $this->createResponse($courses, 200);
        }

        return $this->createResponseError('Cannot found a professor with that id', 404);
    }

    public function store(Request $request, $professor_id)
    {
        $professor = Professor::find($professor_id);

        if ($professor) {
            $this->validation($request);

            $fields = $request->all();
            $fields['professor_id'] = $professor_id;

            Course::create($fields);

            return $this->createResponse('The course was created', 200);
        }
        return createResponseError("Not exist a professor with that id", 404);
    }

    public function update(Request $request, $professor_id, $course_id)
    {
        $professor = Professor::find($professor_id);

        if ($professor) {
            $course = Course::find($course_id);

            if ($course) {
                $this->validation($request);

                $course->title = $request->get('title');
                $course->description = $request->get('description');
                $course->value = $request->get('value');
                $course->professor_id = $professor_id;

                $course->save();

                return $this->createResponse('The course was updated', 200);
            }
            return $this->createResponseError('Cannot found a course with that id', 404);
        }
        return $this->createResponseError('Cannot found a professor with that id', 404);
    }

    public function destroy($professor_id, $course_id)
    {
        $professor = Professor::find($professor_id);

        if ($professor) {
            $courses = $professor->courses();

            if ($courses->find($course_id)) {
                $course = Course::find($course_id);

                $course->students()->detach();

                $course->delete();

                return $this->createResponse("Course deleted", 200);
            }
            return $this->createResponseError('Cannot found a course with this id associate to this professor', 404);
        }
        return $this->createResponseError('Cannot found a professor with that id', 404);
    }

    public function validation($request)
    {
        $rules = [
            'title' => 'required',
            'description' => 'required',
            'value' => 'required|numeric'
        ];

        $this->validate($request, $rules);
    }
}
