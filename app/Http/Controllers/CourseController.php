<?php
namespace App\Http\Controllers;

use App\Course;

class CourseController extends Controller
{
    public function index(){
        $courses = Course::all();
        return $this->createResponse($courses, 200);
    }

    public function show($id){
        $course = Course::find($id);

        if($course){
            return $this->createResponse($course, 200);
        }

        return $this->createResponseError('Course not found', 404);
    }

}
