<?php
namespace App\Http\Controllers;

use App\Professor;
use Illuminate\Http\Request;

class ProfessorController extends Controller
{
    public function index()
    {
        $professors = Professor::all();
        return $this->createResponse($professors, 200);
    }

    public function show($id)
    {
        $professor = Professor::find($id);

        if ($professor) {
            return $this->createResponse($professor, 200);
        }

        return $this->createResponseError('Professor not found', 404);
    }

    public function store(Request $request)
    {
        $this->validation($request);

        Professor::create($request->all());

        return $this->createResponse('The professor has been created', 201);
    }

    public function update(Request $request, $professor_id)
    {
        $professor = Professor::find($professor_id);

        if ($professor) {
            $this->validation($request);

            $name = $request->get('name');
            $address = $request->get('address');
            $phone = $request->get('phone');
            $profession = $request->get('profession');

            $professor->name = $name;
            $professor->address = $address;
            $professor->phone = $phone;
            $professor->profession = $profession;

            $professor->save();

            return $this->createResponse("The professor $professor->id was updated", 200);
        }

        return $this->createResponseError("That professor id not exists", 404);
    }

    public function destroy($professor_id)
    {
        $professor = Professor::find($professor_id);

        if ($professor) {
            if (sizeof($professor->courses) > 0) {
                return $this->createResponseError("That professor has courses. Must delete the courses before delete the professor.", 409);
            }
            $professor->delete();

            return $this->createResponse("The professor $professor->id was deleted", 200);
        }
        return $this->createResponseError("That professor id not exists", 404);
    }

    public function validation($request)
    {
        $rules = [
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required|numeric',
            'profession' => 'required|in:engineer,mathematician,physicist'
        ];

        $this->validate($request, $rules);
    }
}
