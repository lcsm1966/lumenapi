<?php
namespace App\Http\Controllers;

use App\Course;
use App\Student;

class CourseStudentController extends Controller
{
    public function index($course_id)
    {
        $course = Course::find($course_id);

        if ($course) {
            $students = $course->students;

            return $this->createResponse($students, 200);
        }

        return $this->createResponseError('Cannot found a course with that id', 404);
    }

    public function store($course_id, $student_id)
    {
        $course = Course::find($course_id);

        if ($course) {
            $student = Student::find($student_id);

            if ($student) {
                $students = $course->students();

                if ($students->find($student_id)) {
                    return $this->createResponseError("The student $student_id already exists in the $course_id course", 409);
                }
                $course->students()->attach($student_id);
                return $this->createResponse("The student $student_id was added to the course $course_id", 201);
            }
            return createResponseError('Cannot find a student with that id', 404);
        }

        return createResponseError('Cannot find a course with that id', 404);
    }

    public function destroy($course_id, $student_id)
    {
        $course = Course::find($course_id);

        if ($course) {
            $students = $course->students();

            if ($students->find($student_id)) {
                $students->detach($student_id);

                return $this->createResponse("Student deleted", 200);
            }
            return createResponseError('Cannot find a student with that id in this course', 404);
        }
        return createResponseError('Cannot find a course with that id', 404);
    }
}
