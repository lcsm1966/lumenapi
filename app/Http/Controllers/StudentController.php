<?php
namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function index()
    {
        $students = Student::all();
        return $this->createResponse($students, 200);
    }

    public function show($id)
    {
        $student = Student::find($id);

        if ($student) {
            return $this->createResponse($student, 200);
        }

        return $this->createResponseError('Student not found', 404);
    }

    public function store(Request $request)
    {
        $this->validation($request);

        Student::create($request->all());

        return $this->createResponse('The student has been created', 201);
    }

    public function update(Request $request, $student_id)
    {
        $student = Student::find($student_id);

        if ($student) {
            $this->validation($request);

            $name = $request->get('name');
            $address = $request->get('address');
            $phone = $request->get('phone');
            $career = $request->get('career');

            $student->name = $name;
            $student->address = $address;
            $student->phone = $phone;
            $student->career = $career;

            $student->save();

            return $this->createResponse("The student $student->id was updated", 200);
        }

        return $this->createResponseError("That student id not exists", 404);
    }

    public function destroy($student_id)
    {
        $student = Student::find($student_id);

        if ($student) {
            $student->courses()->sync([]);
            $student->delete();

            return $this->createResponse("The student $student->id was deleted", 200);
        }
        return $this->createResponseError("That student id not exists", 404);
    }

    public function validation($request)
    {
        $rules = [
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required|numeric',
            'career' => 'required|in:engineer,mathematician,physicist'
        ];

        $this->validate($request, $rules);
    }
}
