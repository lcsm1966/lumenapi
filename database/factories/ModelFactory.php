<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Professor::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'profession' => $faker->randomElement($array = ['engineer','mathematician','physicist'])
    ];
});

$factory->define(App\Student::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'career' => $faker->randomElement($array = ['engineer','mathematician','physicist'])
    ];
});

$factory->define(App\Course::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence(4),
        'description' => substr($faker->paragraph(4),0,254),
        'value' => $faker->randomFloat($nbMaxDecimals = 2, $min = 100, $max = 1000),
        'professor_id' => mt_rand(1, 50),
    ];
});
